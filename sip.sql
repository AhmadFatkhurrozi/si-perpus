-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 16, 2019 at 12:42 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sip`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `ID_BUKU` varchar(12) NOT NULL,
  `JUDUL` varchar(50) NOT NULL,
  `DESKRIPSI` text NOT NULL,
  `PENERBIT` varchar(50) NOT NULL,
  `THN_TERBIT` varchar(4) NOT NULL,
  `ID_RAK` varchar(12) NOT NULL,
  `ID_KATEGORI` varchar(12) NOT NULL,
  `GAMBAR` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`ID_BUKU`, `JUDUL`, `DESKRIPSI`, `PENERBIT`, `THN_TERBIT`, `ID_RAK`, `ID_KATEGORI`, `GAMBAR`) VALUES
('BK1', 'Jago Android Dalam Seminggu', 'Tutorial Seminggu Menguasai Android Studio...', 'Jombang Dev', '2019', 'RAK1', 'KAT1', '');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `ID_KATEGORI` varchar(12) NOT NULL,
  `NAMA_KATEGORI` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`ID_KATEGORI`, `NAMA_KATEGORI`) VALUES
('KAT1', 'TEKNOLOGI'),
('KAT2', 'BISNIS'),
('KAT3', 'AGAMA'),
('KAT4', 'KESEHATAN');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `ID_PEMINJAMAN` varchar(12) NOT NULL,
  `ID_BUKU` varchar(12) NOT NULL,
  `ID_SISWA` varchar(12) NOT NULL,
  `ID_PETUGAS` varchar(12) NOT NULL,
  `TGL_PINJAM` date NOT NULL,
  `TGL_KEMBALI` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `ID_PETUGAS` varchar(12) NOT NULL,
  `NAMA_PETUGAS` varchar(50) NOT NULL,
  `ALAMAT` text NOT NULL,
  `KONTAK` varchar(20) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`ID_PETUGAS`, `NAMA_PETUGAS`, `ALAMAT`, `KONTAK`, `USERNAME`, `PASSWORD`) VALUES
('PT1', 'Ahmad Fatkhurrozi', 'Kalibening', '085655949778', 'rozi', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `ID_RAK` varchar(12) NOT NULL,
  `NAMA_RAK` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`ID_RAK`, `NAMA_RAK`) VALUES
('RAK1', '1A');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `NIS` varchar(12) NOT NULL,
  `NAMA` varchar(50) NOT NULL,
  `JK` enum('laki-laki','perempuan') NOT NULL,
  `TEMPAT_LAHIR` varchar(20) NOT NULL,
  `TGL_LAHIR` date NOT NULL,
  `NO_HP` varchar(20) NOT NULL,
  `ALAMAT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`NIS`, `NAMA`, `JK`, `TEMPAT_LAHIR`, `TGL_LAHIR`, `NO_HP`, `ALAMAT`) VALUES
('123456789', 'Ahmad Fatkhurrozi', 'laki-laki', 'Jombang', '1997-09-26', '085655949778', 'Kalibening Mojoagung');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`ID_BUKU`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`ID_KATEGORI`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`ID_PEMINJAMAN`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`ID_PETUGAS`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`ID_RAK`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`NIS`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
