<style type="text/css">
	.menu-nav{
		font-size: 16px !important;
	}
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="container-fluid">

		<button type="button" id="sidebarCollapse" class="btn btn-light d-inline-block mx-1 navbar-btn">
			<i class="fas fa-align-justify"></i>
		</button>

		<button class="btn btn-light d-inline-block d-lg-none ml-auto mx-1" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<i class="fas fa-align-justify"></i>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="nav navbar-nav ml-auto text-center">
				<li class="nav-item px-3">
					<a class="nav-link menu-nav" href="#">Pemberitahuan <i class="fa fa-bell"></i></a>
				</li>
			</ul>
		</div>
	</div>
</nav>