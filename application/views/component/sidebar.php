<style type="text/css">
    #sidebar ul li a{
        font-size: 12px !important;
        color: #E7DDDD;
    }

</style>
<nav id="sidebar" style="z-index: 999;">
	<div class="sidebar-header">
		<a href="<?=base_url('admin')?>">
			<h3>Sistem Informasi</h3>
			<strong>SI</strong>
		</a>
	</div>

    <div class="user-panel mx-2 text-center" style="padding: 10px 0;">
        <div class="user-pic">
           	<?php if ($this->session->userdata('auth_foto') != '') { ?>
                <img src="<?=base_url('dist/img/'.$this->session->userdata('auth_foto'));?>" style="border-radius: 50%; object-fit: cover;" height="60px" width="60px" class="img-circle" alt="User Image">
           	<?php } else { ?>
                <img src="<?=base_url('dist/img/foto.jpg');?>" height="60px" width="60px" style="border-radius: 50%; object-fit: cover;" class="img-circle" alt="User Image">
			<?php } ?>   
        </div>

        <div class="user-info my-2" style="font-size: 9px;">
			<span class="user-name">
				<strong><?=$this->session->userdata('auth_nama'); ?></strong>
			</span><br>
			<span class="user-status">
				<i class="fa fa-circle text-success"></i>
				<span>Online</span>
			</span>
		</div>
    </div>

    <ul class="list-unstyled components">
        <li class="<?php echo __menu_active('Dashboard', $title2 ); ?>">
            <a href="<?=base_url('admin')?>">
                <i class="fas fa-home fa-fw"></i> Dashboard
            </a>
        </li>

        <li class="<?php echo __menu_active('Pengguna', $title2 ); ?>">
            <a href="#userSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-users"></i> Pengguna
            </a>
            <ul class="collapse list-unstyled <?php if($title2 == 'Pengguna') echo 'show' ?>" id="userSubmenu">
                <li class="<?php echo __menu_active('Admin', $title3 ); ?>"><a href="<?=base_url('user-admin')?>">
                	<i class="fa fa-angle-double-right"></i> Admin</a>
                </li>
            </ul>
        </li>
    </ul>

    <ul class="list-unstyled">
        <li>
            <a href="<?php echo base_url('admin/logout'); ?>" class="text-center" onclick="return confirm('Keluar ?')"><i class="fa fa-sign-out-alt"></i> Keluar</a>
        </li>
    </ul>
</nav>