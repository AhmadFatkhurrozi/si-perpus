<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Sistem Informasi</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<?php 
    echo __css('fontawesome');
    echo __css('bootstrap');
    echo __css('fontastic');
    echo __css('default');
    echo __css('custom');
    echo __css('style4');

    echo __js('solid');
    echo __js('fontawesome5');
?>