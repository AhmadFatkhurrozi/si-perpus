<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
  </head>
  <body>
    <div class="wrapper">
        <?php $this->load->view("component/sidebar.php") ?>

        <div id="content">
            <?php $this->load->view("component/navbar.php") ?>


            <h2 class="pb-3"><?=$title1;?></h2>

            <a href="" class="btn btn-primary btn-sm mb-2"><i class="fa fa-plus"></i> Tambah</a>

            <table class="table table-inverse">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

      </div>

        <?php $this->load->view("component/footer.php") ?>
    </div>

   <?php  $this->load->view("component/js.php") ?>
   <script type="text/javascript">
      $(document).ready(function () {
          $('#sidebarCollapse').on('click', function () {
              $('#sidebar').toggleClass('active');
          });
      });
    </script>
  </body>
</html>