<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        
    }
    
    function auth($username, $pass)
    {
        $this->db->where('USERNAME', $username);
        $this->db->where('PASSWORD',md5($pass));
        $query = $this->db->get('users');
        return $query;
    }
    
    function tampil_data($table){
        return $this->db->get($table);
    }

}

/* End of file M_data.php */
/* Location: ./application/models/M_data.php */